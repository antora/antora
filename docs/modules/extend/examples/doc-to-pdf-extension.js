'use strict'

const fs = require('node:fs')
const { promises: fsp } = fs
const ospath = require('node:path')
const { posix: path } = ospath
const { execFile } = require('node:child_process')
const { PassThrough } = require('node:stream')

const IS_WIN = process.platform === 'win32'
const LIBREOFFICE_WIN = 'C:\\Program Files\\LibreOffice\\program\\soffice.exe'

module.exports.register = function ({ config }) {
  this.once('contentClassified', async ({ playbook, contentCatalog, filesRepo }) => {
    const logger = this.getLogger('doc-to-pdf-extension')
    let command = config.command || { windows: './docto.exe', linux: 'libreoffice' }
    if (command.constructor === Object) command = command[IS_WIN ? 'windows' : 'linux']
    if (IS_WIN) {
      if (command === 'libreoffice') {
        command = LIBREOFFICE_WIN
      } else if (!command.endsWith('.exe')) {
        command += '.exe'
      }
    }
    if (command.startsWith('./')) command = ospath.join(playbook.dir, command)
    const libreoffice = ospath.basename(command) !== 'docto.exe'
    const docExtnames = (config.extensions || ['.docx']).reduce((accum, it) => Object.assign(accum, { [it]: true }), {})
    const batchDirs = []
    const batchSize = (config.batchSize ?? 150) || Number.MAX_SAFE_INTEGER
    const sourceOidMapping = filesRepo ? filesRepo.sourceOidMapping : {}
    const filesToConvert = contentCatalog.findBy({ family: 'attachment' }).filter((file) => {
      const { src } = file
      const extname = src.extname
      if (!docExtnames[extname]) return
      const staticPdfFile = contentCatalog.getById(
        Object.assign({}, src, { relative: src.relative.slice(0, -extname.length) + '.pdf' })
      )
      if (staticPdfFile) contentCatalog.removeFile(staticPdfFile)
      if (src.oid && src.oid === sourceOidMapping[file.out.path.slice(0, -extname.length) + '.pdf']) {
        coerceToPdf(file, null) // null contents means to keep file in files repo
        return
      }
      if (libreoffice && staticPdfFile) {
        coerceToPdf(file, staticPdfFile.contents)
        return
      }
      return true
    })
    if (!filesToConvert.length) return
    logger.info(`Converting ${filesToConvert.length} doc files to pdf (batch size: ${batchSize})`)
    const batchDirBase = ospath.join(playbook.dir, 'build/doc-to-pdf')
    const convertArgs = libreoffice
      ? ['--writer', '--headless', '--norestore']
          .concat(IS_WIN ? ['--safe-mode', '--print-to-file', '--printer-name', 'pdf', '--outdir', '.'] : ['--convert-to', 'pdf'])
      : ['-WD', '-f', '.', '-o', '.', '--no-subdirs', '-t', 'wdFormatPDF', '--PDF-no-BitmapMissingFonts']
    try {
      let i = 0
      for (let offset = 0, tot = filesToConvert.length; offset < tot; offset += batchSize) {
        const batchDir = batchDirBase + `-${++i}`
        batchDirs.push(batchDir)
        const convertOpts = { cwd: batchDir }
        if (IS_WIN) Object.assign(convertOpts, { windowsHide: true })
        await fsp.mkdir(batchDir, { recursive: true })
        const filesToConvertBatch = filesToConvert.slice(offset, offset + batchSize)
        const filepathsToConvert = await Promise.all(filesToConvertBatch.map((file) => {
          const sourceRelpath = makeSourceFilename(file.src)
          return fsp.writeFile(ospath.join(batchDir, sourceRelpath), file.contents).then(() => sourceRelpath)
        }))
        const convertArgsBatch = libreoffice ? convertArgs.slice().concat(filepathsToConvert) : convertArgs
        await new Promise((resolve, reject) => {
          logger.info([command].concat(convertArgsBatch).concat(`(dir: ${batchDir})`).join(' '))
          execFile(command, convertArgsBatch, convertOpts, (err, stdout, stderr) => {
            if (err) {
              stderr = stderr.trimEnd()
              stdout = stdout.trimEnd()
              const splitIdx = libreoffice && stderr.indexOf('Usage: ')
              if (~splitIdx) stderr = stderr.slice(0, splitIdx).trimEnd()
              if (stderr) err.message += stderr
              if (stdout) err.message += stdout
              reject(err)
            } else if (!libreoffice && stdout.includes('Error: ')) {
              reject(new Error(stdout.trimEnd().split(/\n\r?/).find((it) => it.startsWith('Error: ')).slice(7)))
            } else {
              resolve()
            }
          })
        })
        filesToConvertBatch.forEach((file) => {
          const pdfRelpath = makeSourceFilename(file.src).slice(0, -file.src.extname.length) + '.pdf'
          const contents = new LazyReadable(() => fs.createReadStream(ospath.join(batchDir, pdfRelpath)))
            .on('error', (err) => logger.error(err, 'failed to convert ' + makeSourceFilename(file.src)))
          coerceToPdf(file, contents)
        })
      }
    } catch (err) {
      logger.error(err, 'failed to convert doc files to pdf')
    } finally {
      this.once('contextClosed', async () => {
        for (const batchDir of batchDirs) await fsp.rm(batchDir, { recursive: true, force: true })
      })
    }
  })
}

function coerceToPdf (file, contents) {
  file.contents = contents
  const extnameLength = file.src.extname.length
  file.out.basename = file.out.basename.slice(0, -extnameLength) + '.pdf'
  file.out.path = path.join(file.out.dirname, file.out.basename)
  file.pub.url = file.pub.url.slice(0, -extnameLength) + '.pdf'
}

function makeSourceFilename (src) {
  return `${src.component}-${src.module}-${src.relative.replaceAll('/', '-')}`
}

// adapted from https://github.com/jpommerening/node-lazystream/blob/master/lib/lazystream.js | license: MIT
class LazyReadable extends PassThrough {
  constructor (fn, options) {
    super(options)
    this._read = function () {
      delete this._read // restores original method
      fn.call(this, options).on('error', this.emit.bind(this, 'error')).pipe(this)
      return this._read.apply(this, arguments)
    }
    this.emit('readable')
  }
}
