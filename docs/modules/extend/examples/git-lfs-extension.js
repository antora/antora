'use strict'

const { execFile } = require('node:child_process')
const fsp = require('node:fs/promises')
const ospath = require('node:path')

module.exports.register = function ({ config }) {
  this.once('playbookBuilt', async ({ playbook }) => {
    const lfsContentSources = playbook.content.sources.filter((it) => it.lfs)
    if (!lfsContentSources.length) return
    let workspaceDir = config.workspaceDir ||
      ((playbook.runtime.cacheDir || './.cache/antora') + ospath.sep + 'content-with-lfs')
    workspaceDir = ospath.join(workspaceDir.startsWith('./') ? playbook.dir : process.cwd(), workspaceDir)
    await fsp.mkdir(workspaceDir, { recursive: true })
    workspaceDir = './' + ospath.relative(playbook.dir, workspaceDir)
    for (const contentSource of lfsContentSources) {
      contentSource.remoteUrl = contentSource.url
      contentSource.url = workspaceDir + ospath.sep + contentSource.url.match(/([^\/]+?)(?:\.git)$/)[1]
    }
    this.updateVariables({ playbook })
  })

  this.once('beforeProcess', async ({ playbook }) => {
    const lfsContentSources = playbook.content.sources.filter((it) => it.lfs)
    if (!lfsContentSources.length) return
    for (const contentSource of lfsContentSources) {
      const cloneDir = ospath.join(playbook.dir, contentSource.url)
      const cached = await fsp.stat(cloneDir).then((stat) => stat.isDirectory(), () => false)
      let gitCmd
      const gitArgs = []
      let gitCwd
      if (cached) {
        if (playbook.runtime.fetch) gitCmd = 'pull'
        gitCwd = cloneDir
      } else {
        gitCmd = 'clone'
        gitArgs.push('-q', contentSource.remoteUrl, cloneDir)
      }
      if (gitCmd) {
        console.log(`[${gitCmd}] ${contentSource.remoteUrl}`)
        await git(gitCmd, gitArgs, gitCwd)
      }
      await git('lfs', ['ls-files', '-l'], cloneDir)
        .then((data) => fsp.writeFile(ospath.join(cloneDir, '.git', 'lfs', 'head-files'), data + '\n'))
    }
  })

  this.once('contentClassified', async ({ contentCatalog }) => {
    const gitLfsFilesByWorktree = {}
    for (const file of contentCatalog.getFiles()) {
      const worktree = file.src.origin.worktree
      if (!worktree) continue
      if (!(worktree in gitLfsFilesByWorktree)) {
        gitLfsFilesByWorktree[worktree] = await fsp
          .readFile(ospath.join(worktree, '.git', 'lfs', 'head-files'), 'utf8')
          .then(
            (data) => data.trimEnd().split('\n').reduce((accum, line) => {
              const [oid, filepath] = line.split(/ [*-] /, 2)
              return Object.assign(accum, { [filepath]: oid })
            }, {}),
            () => ({})
          )
      }
      file.src.oid = gitLfsFilesByWorktree[worktree][file.src.path]
    }
  })
}

function git (command, args, cwd) {
  return new Promise((resolve, reject) => {
    execFile('git', [command].concat(args), { cwd }, (err, stdout) => err ? reject(err) : resolve(stdout.trimEnd()))
  })
}
