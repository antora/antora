'use strict'

const { execFile } = require('node:child_process')
const fsp = require('node:fs/promises')
const ospath = require('node:path')

module.exports.register = function () {
  if (!this.getVariables().playbook.env.CI_PROJECT_URL) return

  this.once('beforeProcess', async ({ playbook }) => {
    const branch = 'files'
    const filesProjectUrl = playbook.env.CI_PROJECT_URL
    const filesRepo = {
      projectUrl: filesProjectUrl,
      url: filesProjectUrl + '.git',
      branch,
      path: (playbook.runtime.cacheDir || './cache/antora') + ospath.sep + 'files',
    }
    let gitCmd
    const gitArgs = []
    let gitCwd
    if (await fsp.stat(filesRepo.path).then((stat) => stat.isDirectory(), () => false)) {
      if (playbook.runtime.fetch) gitCmd = 'pull'
      gitCwd = filesRepo.path
    } else {
      gitCmd = 'clone'
      gitArgs.push('-q', '-b', branch, filesRepo.url, ospath.basename(filesRepo.path))
      gitCwd = ospath.dirname(filesRepo.path)
    }
    if (gitCmd) {
      console.log(`[${gitCmd}] ${filesRepo.url} (${branch})`)
      await git(gitCmd, gitArgs, gitCwd)
    }
    filesRepo.sourceOidMapping = await fsp.readFile(ospath.join(filesRepo.path, '.source-oid-mapping'), 'utf8').then(
      (data) => data.trimEnd().split('\n').reduce((accum, line) => {
        const [oid, filepath] = line.split(' * ', 2)
        if (filepath) accum[filepath] = oid
        return accum
      }, {}),
      () => ({})
    )
    this.updateVariables({ filesRepo })
  })

  this.once('contentClassified', async ({ contentCatalog, filesRepo }) => {
    const { path: filesRepoPath, sourceOidMapping } = filesRepo
    const updatedSourceOidMapping = {}
    const dirs = new Set(['.'])
    for (const file of contentCatalog.findBy({ family: 'attachment' })) {
      const relativeOutPath = file.out.path
      const oid = file.src.oid
      if (!oid || oid !== sourceOidMapping[relativeOutPath]) {
        const absOutPath = ospath.join(filesRepoPath, relativeOutPath)
        await mkdirp(ospath.dirname(absOutPath), ospath.dirname(relativeOutPath), dirs)
        await fsp.writeFile(absOutPath, file.contents)
      }
      updatedSourceOidMapping[relativeOutPath] = oid
      Object.assign(file, { contents: null, out: undefined })
    }
    for (const filepath of Object.keys(sourceOidMapping)) {
      if (!(filepath in updatedSourceOidMapping)) await fsp.rm(ospath.join(filesRepoPath, filepath))
    }
    await fsp.writeFile(
      ospath.join(filesRepoPath, '.source-oid-mapping'),
      Object.entries(updatedSourceOidMapping).sort(([a], [b]) => a.localeCompare(b, 'en-US', { numeric: true }))
        .reduce((accum, [filepath, oid]) => accum + (oid ? `${oid} * ${filepath}\n` : ''), '')
    )
  })

  this.once('beforePublish', async ({ playbook, siteCatalog, filesRepo }) => {
    const { branch: filesRepoBranch, path: filesRepoPath, projectUrl: filesRepoProjectUrl, url: filesRepoUrl } = filesRepo
    const siteUrl = playbook.site.url || playbook.env.CI_PAGES_URL
    let sitePath = siteUrl ? new URL(siteUrl).pathname : '/'
    if (sitePath === '/') sitePath = ''
    const redirectsOutPath = '_redirects'
    let redirectsContents = [
      `${sitePath}/:component/_attachments/* ${filesRepoProjectUrl}/-/raw/${filesRepoBranch}/:component/_attachments/:splat 302`,
      `${sitePath}/:component/:module/_attachments/* ${filesRepoProjectUrl}/-/raw/${filesRepoBranch}/:component/:module/_attachments/:splat 302`,
    ].join('\n')
    const redirectsFile = siteCatalog.getFiles().find((it) => it.out.path === redirectsOutPath)
    if (redirectsFile) redirectsContents += '\n' + redirectsFile.contents.toString().trimEnd() + '\n'
    siteCatalog.addFile({ contents: Buffer.from(redirectsContents + '\n'), out: { path: redirectsOutPath } })
  })

  this.once('sitePublished', async ({ playbook, filesRepo }) => {
    const { branch: filesRepoBranch, path: filesRepoPath, url: filesRepoUrl } = filesRepo
    if (playbook.env.GITLAB_USER_EMAIL) {
      await git('config', ['user.email', playbook.env.GITLAB_USER_EMAIL], filesRepoPath)
      await git('config', ['user.name', playbook.env.GITLAB_USER_NAME], filesRepoPath)
    }
    await git('add', ['.'], filesRepoPath)
      .then(() => git('status', ['-s'], filesRepoPath))
      .then((changes) => changes && git('commit', ['-m', 'publish files'], filesRepoPath))
    if (playbook.env.PUSH_ACCESS_TOKEN) {
      await git('push', [filesRepoUrl.replace('//', `//oauth2:${playbook.env.PUSH_ACCESS_TOKEN}@`), filesRepoBranch], filesRepoPath)
    }
  })
}

function git (command, args, cwd) {
  return new Promise((resolve, reject) => {
    execFile('git', [command].concat(args), { cwd }, (err, stdout) => err ? reject(err) : resolve(stdout.trimEnd()))
  })
}

async function mkdirp (absDir, relativeDir, existing) {
  if (relativeDir in existing) return
  await fsp.mkdir(absDir, { recursive: true })
  existing.add(relativeDir)
  while ((relativeDir = ospath.dirname(relativeDir)) !== '.') existing.add(relativeDir)
}
